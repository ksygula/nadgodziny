<?php


namespace App\Service;


use App\Entity\Overtime;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class OvertimeService extends AbstractService
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->model = $em->getRepository(Overtime::class);
    }

    public function saveOvertime(Overtime $overtime, User $user)
    {
        $overtime->setUser($user);
        $this->updateHours($overtime);
        $overtime->setAddDate(new \DateTime());
        $this->save($overtime);
    }

    public function getOvertimeInfoForUser(User $user)
    {
        return $this->model->findBy(array('user' => $user->getId()), array('id' => 'DESC'));
    }

    public function getLastOvertimeForUser(User $user)
    {
        return $this->model->findOneBy(['user' => $user->getId()], array('id' => 'DESC'));
    }

    public function updateHours(Overtime $overtime)
    {
        $lastOvertime = $this->getLastOvertimeForUser($overtime->getUser());
        if ($lastOvertime) {
            $updateHours = $lastOvertime->getHours() + $overtime->getAmount();
            $overtime->setHours($updateHours);
        } else {
            $overtime->setHours($overtime->getAmount());
        }
    }
}