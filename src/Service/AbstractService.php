<?php


namespace App\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class AbstractService
{
    /**
     * @var EntityRepository
     */
    protected $model;
    protected $em;
    /**
     * @param EntityManager $em
     * @param $entityName
     */
    protected function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }
    protected function save($object)
    {
        $this->em->persist($object);
        $this->em->flush();
    }
    protected function delete($object)
    {
        $this->em->remove($object);
        $this->em->flush();
    }
    protected function entityManager()
    {
        return $this->em;
    }
}