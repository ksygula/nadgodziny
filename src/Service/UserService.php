<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService extends AbstractService
{
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->model = $em->getRepository(User::class);
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getUsers() {
        return $this->model->findAll();
    }

    public function saveUser(User $user, $formPassword) {
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $formPassword
            )
        );
        $user->setRoles(['ROLE_USER']);

        $this->save($user);
    }
}