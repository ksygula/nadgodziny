<?php

namespace App\Controller;

use App\Entity\Overtime;
use App\Entity\User;
use App\Form\OvertimeFormType;
use App\Service\OvertimeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OvertimeController
 * @package App\Controller
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class OvertimeController extends AbstractController
{

    /**
     * @param Request $request
     * @param OvertimeService $overtimeService
     * @return Response
     * @Route("/", name="main_page", methods={"GET", "POST"})
     */
    public function index(Request $request, OvertimeService $overtimeService)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $overtime = new Overtime();

        $form = $this->createForm(OvertimeFormType::class, $overtime);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $overtime = $form->getData();
            $overtimeService->saveOvertime($overtime, $user);

            return $this->redirectToRoute('main_page');
        }

        $overtimes = $overtimeService->getOvertimeInfoForUser($user);

        return $this->render('overtime/index.html.twig', array(
            'user'=>$user,
            'overtimes'=>$overtimes,
            'form' => $form->createView()));
    }



}