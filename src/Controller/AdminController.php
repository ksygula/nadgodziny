<?php


namespace App\Controller;


use App\Service\OvertimeService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OvertimeController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/users", name="admin_users", methods={"GET"})
     * @param UserService $userService
     * @param OvertimeService $overtimeService
     * @return Response
     */
    public function users(UserService $userService, OvertimeService $overtimeService) {
        $users = $userService->getUsers();

        $usersData = array();
        foreach ($users as $user) {
            $usersData[] = array($user, $overtimeService->getLastOvertimeForUser($user));
        }

        return $this->render('users/users.html.twig', array('usersData'=>$usersData));
    }
}