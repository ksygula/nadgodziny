<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Imię',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('secondName', TextType::class, [
                'label' => 'Nazwisko',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Oba hasła muszą być identyczne',
                'mapped' => false,
                'error_mapping' => ['.' => 'second'],
                'first_options'  => array(
                    'label' => 'Hasło',
                    'attr' =>array('class' => 'form-control')
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło',
                    'attr' =>array('class' => 'form-control')
                ),
                'options' => array(
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Proszę uzupełnić hasło',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Hasło musi mieć conajmniej {{ limit }} znaków',
                            'max' => 4096,
                        ]),
                    ],
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
