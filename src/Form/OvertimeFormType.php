<?php


namespace App\Form;


use App\Entity\Overtime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OvertimeFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',  DateType::class, [
                'label' => 'Data nadgodzin',
                'widget' => 'single_text',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('amount', NumberType::class, [
                'label' => 'Ilość nadgodzin',
                'attr' =>array('class' => 'form-control')
            ])
            ->add('description', TextType::class, [
                'label' => 'Opis',
                'attr' =>array('class' => 'form-control')
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Overtime::class,
        ]);
    }
}