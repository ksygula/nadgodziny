<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="Użytkownik z tym adresem email już istnieje")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Regex(
     *     pattern="/^[a-z ,.'-]+$/i",
     *     match=true,
     *     message="Użyj prawdziwego imienia"
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Regex(
     *     pattern="/^[a-z ,.'-]+$/i",
     *     match=true,
     *     message="Użyj prawdziwego nazwiska"
     * )
     */
    private $secondName;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Overtime", mappedBy="user", orphanRemoval=true)
     */
    private $overtimes;

    public function __construct()
    {
        $this->overtimes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param mixed $secondName
     */
    public function setSecondName($secondName): void
    {
        $this->secondName = $secondName;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Overtime[]
     */
    public function getOvertimes(): Collection
    {
        return $this->overtimes;
    }

    public function addOvertime(Overtime $overtime): self
    {
        if (!$this->overtimes->contains($overtime)) {
            $this->overtimes[] = $overtime;
            $overtime->setUser($this);
        }

        return $this;
    }

    public function removeOvertime(Overtime $overtime): self
    {
        if ($this->overtimes->contains($overtime)) {
            $this->overtimes->removeElement($overtime);
            // set the owning side to null (unless already changed)
            if ($overtime->getUser() === $this) {
                $overtime->setUser(null);
            }
        }

        return $this;
    }
}
